let Vue = require('vue')

Vue.component('app', require('./components/App').default)
Vue.component('search', require('./components/Search').default)
Vue.component('list-view', require('./components/ListView').default)
Vue.component('detail-view', require('./components/DetailView').default)
Vue.component('audio-view', require('./components/AudioView').default)
Vue.component('loader', require('./components/Loader').default)
Vue.component('single-list-item', require('./components/SingleListItem').default)

let EventBus = new Vue()

export default EventBus

let app = new Vue({
    el:"#app"
})